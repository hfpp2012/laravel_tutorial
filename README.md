# Ruby 程序员学习 laravel 框架笔记

本教程是从ruby的视脚来学习laravel这个神奇的php框架，适用于ruby程序员和php程序员浏览。

写这本教程我使用的laravel版本是5.3，新的版本也应该能适用。

原文发布于我的个人博客：https://www.rails365.net

源码位于：https://github.com/yinsigan/laravel_tutorial

电子版: [PDF](https://www.gitbook.com/download/pdf/book/yinsigan/laravel_tutorial) [Mobi](https://www.gitbook.com/download/mobi/book/yinsigan/laravel_tutorial) [ePbu](https://www.gitbook.com/download/epub/book/yinsigan/laravel_tutorial)

### 联系我:

email: hfpp2012@gmail.com

qq: 903279182

欢迎关注我的个人订阅号:

![](https://rails365.oss-cn-shenzhen.aliyuncs.com/uploads/photo/image/310/2017/0f6c7b070c711c48dbe92193f71e9cbf.jpg)
